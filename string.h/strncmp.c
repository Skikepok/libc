#include <string.h>

int strncmp(const char *s1, const char *s2, size_t n)
{
  for (int i = 0; i < n && (s1[i] || s2[i]); i++)
  {
    if (s1[i] > s2[i])
      return 1;

    if (s1[i] < s2[i])
      return 1;
  }

  return 0;
}
