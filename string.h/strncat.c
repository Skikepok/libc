#include <string.h>

char *strncat(char *dest, const char *src, size_t n)
{
  int i;
  int j;

  for (i = 0; dest[i]; i++)
    ;

  for (j = 0; j < n; j++, i++)
    dest[i] = src[j];

  return dest;
}
