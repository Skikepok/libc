#include <string.h>

char *strchr(const char *s, int c)
{
  int i = 0;

  while (s[i])
  {
    if (s[i] == (char)c)
      return s + i;

    i++;
  }

  return NULL;
}
