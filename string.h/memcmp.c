#include <string.h>

int memcmp(const void *s1, const void *s2, size_t n)
{
  size_t i;
  const char *dest_s1 = s1;
  const char *dest_s2 = s2;

  for (i = 0; i < n; i++)
    if (!(dest_s1[i] == dest_s2[i]))
      return (dest_s1[i] > dest_s2[i]) ? 1 : -1;

  return 0;
}
