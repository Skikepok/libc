#include <string.h>

void *memcpy(void *dest, const void *src, size_t n)
{
  const char *src_char = src;
  char *dest_char = dest;
  int i;

  for (i = 0; i < n; i++)
    dest_char[i] = src_char[i];

  return dest;
}
