#include <string.h>

char *strcat(char *dest, const char *src)
{
  int i;
  int j;

  for (i = 0; dest[i]; i++)
    ;

  for (j = 0; src[j]; j++, i++)
    dest[i] = src[j];

  return dest;
}
