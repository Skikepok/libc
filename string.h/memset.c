#include <string.h>

void *memset(void *s, int c, size_t n)
{
  char *dest = s;
  int i;

  for (i = 0; i < n; i++)
    dest[i] = c;

  return s;  
}
