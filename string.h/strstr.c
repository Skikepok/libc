#include <string.h>

char *strstr(const char *haystack, const char *needle)
{
  int needle_size = strlen(needle);
  int i = 0;

  while (haystack[i])
  {
    if (haystack[i] == *needle && !strncmp(haystack + i, needle, needle_size))
      return haystack + i;
    
    i++;
  }
  
  return NULL;
}
