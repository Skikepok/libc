#include <string.h>

void *memmove(void *dest, const void *src, size_t n)
{
  char tmp[n];
  const char *c_src = src;
  char *c_dest = dest;

  for (int i = 0; i < n; i++)
    tmp[i] = c_src[i];

  for(int i = 0; i < n; i++)
    c_dest[i] = tmp[i];

  return dest;
}
