#include <string.h>

char *strrchr(const char *s, int c)
{
  int i = 0;
  char *last = NULL;

  while (s[i])
  {
    if (s[i] == (char)c)
      last = s + i;

    i++;
  }

  return last;
}
